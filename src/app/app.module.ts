import { NgbModule, NgbDropdownModule, NgbDatepicker, NgbDatepickerModule, NgbCalendarGregorian } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule, DecimalPipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { BsComponentModule } from './layout/bs-component/bs-component.module';
import { MatSliderModule } from '@angular/material/slider';
import {MatDialogModule} from '@angular/material/dialog';
import { ToastrModule } from 'ngx-toastr';
import { Auth1Guard } from './shared/guard/company/auth1.guard';
import { LandingPageComponent } from './layout/landingPage/landing-page/landing-page.component';
import { NgxSpinnerModule, NgxSpinnerService } from "ngx-spinner";




@NgModule({
    imports: [
    CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LanguageTranslationModule,
        AppRoutingModule,
        NgbModule,
        NgbDropdownModule,
        BsComponentModule,
        MatSliderModule,
        MatDialogModule,
        NgbDatepickerModule,
        NgxSpinnerModule,
        // DecimalPipe,
        
        ToastrModule.forRoot({
            timeOut: 1500,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            // progressBar:true,
            closeButton: true,
          })

        
     

    
    ],
    declarations: [AppComponent, LandingPageComponent],
    providers: [AuthGuard, NgbCalendarGregorian, DecimalPipe, Auth1Guard, NgxSpinnerService],
    bootstrap: [AppComponent]
})
export class AppModule {}
