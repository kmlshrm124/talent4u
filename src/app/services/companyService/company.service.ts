import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor( private http: HttpClient) { }

  public companyRegister(obj){
    return this.http.post(environment.base_url+"company/register",obj);
  }

public companyLogin(obj){

  return this.http.post(environment.base_url+"login",obj);
}

public getCompanyProfilelById(id){

  let params = new HttpParams();
    params = params.append('id',id);
    return this.http.get(environment.base_url+"company", {params:params});
}
public companyProfileUpdate(obj){
  return this.http.post(environment.base_url+"company/updateProfile",obj);
}
public createPosition(obj){
    return this.http.post(environment.base_url+"company/createPosition",obj);
  }
  public getAllPositions(obj){
    return this.http.post(environment.base_url+"company/getPositions",obj);
  }
  public deleteOpenPosition(positionObj){
      return this.http.post(environment.base_url+"company/deletePositions",positionObj);
  }

}