import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  registerKey = new BehaviorSubject({});


  constructor( private http:HttpClient ) { }

 public studentLogin(obj){
  // let params = new HttpParams();
  // params = params.append('type', type);
    return this.http.post(environment.base_url+"login",obj);
  }

  public forgotPassword(obj){
    return this.http.post(environment.forgotPassword_url,obj);
  }

  public validateSecretKey(obj){
    return this.http.post(environment.validateSecretKey_url,obj);
  }
  public resetPassword(obj){
    return this.http.post(environment.resetPassword_url, obj);
  }
  public refreshApi(){
    return this.http.get(environment.refresh_url);
  }

  public studentProfile(id){
    let params = new HttpParams();
    params = params.append('id',id);
    return this.http.get(environment.base_url+"student", {params:params});
  }

  public studentProfileSection1(obj){
    return this.http.post(environment.base_url+"student/updateProfile",obj);
  }
  public logout(){
    if(!localStorage.getItem('key') || !localStorage.getItem('companyLogIn') ){
      return true;
    }
    else{
      return false;
    }
  }
  public setRegisterKey(key){
    localStorage.setItem("RegisterKey", key);
    this.registerKey.next(key); 
  }
public getRegisterKey(){
  return this.registerKey;
}


}
