import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Auth1Guard implements CanActivate  {
  
  constructor(private router: Router) {}

  canActivate() {
      if (localStorage.getItem('companyLogIn')) {
          return true;
      }
      this.router.navigate(['/dashboard']);
      return false;
  }
}

