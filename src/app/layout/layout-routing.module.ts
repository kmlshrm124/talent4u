import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { StudentregisterationComponent } from './registeration/studentRegistration/studentregisteration/studentregisteration.component';
import { StuDashboardComponent } from './bs-component/studentLogin/studentDashboard/stu-dashboard/stu-dashboard.component';
import { AuthGuard } from '../shared';
import { CompanyDashboardComponent } from './company/company-dashboard/company-dashboard.component';
import { CompanyPositionsComponent } from './company/company-positions/company-positions.component';
import { Auth1Guard } from '../shared/guard/company/auth1.guard';
import { LandingPageComponent } from './landingPage/landing-page/landing-page.component';





const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
            { path: 'charts', loadChildren: () => import('./charts/charts.module').then(m => m.ChartsModule) },
            { path: 'tables', loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule) },
            { path: 'forms', loadChildren: () => import('./form/form.module').then(m => m.FormModule) },
            { path: 'bs-element', loadChildren: () => import('./bs-element/bs-element.module').then(m => m.BsElementModule) },
            { path: 'grid', loadChildren: () => import('./grid/grid.module').then(m => m.GridModule) },
            { path: 'components', loadChildren: () => import('./bs-component/bs-component.module').then(m => m.BsComponentModule) },
            { path: 'blank-page', loadChildren: () => import('./blank-page/blank-page.module').then(m => m.BlankPageModule) },
        ]
    },
    {
        path:'registration/:id', component:StudentregisterationComponent
    },
    {
        path:'studentDashboard', component:StuDashboardComponent, canActivate: [AuthGuard]
    },
    {
        path:'companyDashboard', component:CompanyDashboardComponent, canActivate: [Auth1Guard]
    },
    {
        path:'companyDashboard/positions/:id', component:CompanyPositionsComponent, canActivate: [Auth1Guard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class LayoutRoutingModule {}
