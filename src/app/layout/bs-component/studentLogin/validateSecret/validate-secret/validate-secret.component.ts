import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login/login.service';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ResetPasswordComponent } from '../../resetPassword/reset-password/reset-password.component';
import { NotificationService } from '../../../../../services/notification.service';




@Component({
  selector: 'app-validate-secret',
  templateUrl: './validate-secret.component.html',
  styleUrls: ['./validate-secret.component.scss']
})
export class ValidateSecretComponent implements OnInit {
  submitted: boolean = false;
  stuRegisterForm: FormGroup;
  mail:any;
  validateSecretKeyResponse:any;
@Input() obj;
  @ViewChild("closebutton", { static: true }) closebuttons;
  constructor(private fb: FormBuilder, private loginService: LoginService, private modalService:NgbModal,
    private notify:NotificationService, private NgbActiveModal:NgbActiveModal) {}

  ngOnInit() {
    console.log("object console", this.obj);
    this.stuRegisterForm = this.fb.group({
      secretKey: ["", [Validators.required]],
    
    });
    // console.log(this.stuRegisterForm.value);
  }
 
  /**
   *
   * @param isValid
   * @param formValue
   * to save gamesForm
   */
  save(isValid, formValue) {
    // this.spinner.show();
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    const formObj = {
     
      email: this.obj.email,
      secretKey:formValue.secretKey,
      type:this.obj.type,
    };
    console.log(formObj);
    // this.mail =formObj.email

    this.loginService.validateSecretKey(formObj).subscribe(
      (res: any) => {
        this.validateSecretKeyResponse = (res);
        if(res.statusCode == 200){
          console.log(res);
          this.notify.showSuccess(res.body,"");
    this.NgbActiveModal.close();
          const modalRef = this.modalService.open(ResetPasswordComponent);
          modalRef.componentInstance.obj = formObj;

        } else{
          this.notify.showError(res.body,"");
        }
      },
      (error) => {
        this.notify.showError(error.body,"");
      }
    );
  }

  close(){
    this.NgbActiveModal.close();
  }
  resendMail(){
    if(this.validateSecretKeyResponse){
      let formObj={
        email:this.obj.email,
        type:this.obj.type,
      }
      this.loginService.forgotPassword(formObj).subscribe((res:any)=>{
        if(res.statusCode == 200){
          // alert("you will get message on mail");
          console.log("forgot password response", res.body);
          this.notify.showWarning("You will get message on mail soon!!","");
        }else{
          this.notify.showError(res.body,"");
  
        }
      },(error)=>{
        this.notify.showError(error.body,"");
      });
    }
    else{
      this.notify.showError("Please Enter Secret Key","");
    }

  }

}
