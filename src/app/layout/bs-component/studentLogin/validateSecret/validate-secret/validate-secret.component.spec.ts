import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateSecretComponent } from './validate-secret.component';

describe('ValidateSecretComponent', () => {
  let component: ValidateSecretComponent;
  let fixture: ComponentFixture<ValidateSecretComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateSecretComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateSecretComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
