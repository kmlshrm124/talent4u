import { Component, OnInit, Inject, Input } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { LoginService } from "../../../../services/login/login.service";
import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalComponent } from "../../components";
import { ValidateSecretComponent } from "../validateSecret/validate-secret/validate-secret.component";
import { NotificationService } from "../../../../services/notification.service";
import { MatDialog } from "@angular/material/dialog";
import { Router, NavigationExtras } from "@angular/router";
import { CompanyService } from 'src/app/services/companyService/company.service';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  submitted: boolean = false;
  stuRegisterForm: FormGroup;
  mail: any;
  loginResponse: any;
  loginResponseId: any;
  // type:number;
  @Input() public type;

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private modalService: NgbModal,
    private notifyService: NotificationService,
    private NgbActiveModal: NgbActiveModal,
    private router: Router,
    private companyService: CompanyService
  ) {
    this.router.navigateByUrl('/dashboard');
  }

  ngOnInit() {
    console.log("user console", this.type);
    this.stuRegisterForm = this.fb.group({
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),
        ],
      ],
      password: ["", [Validators.required, Validators.minLength(8)]],
    });
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save gamesForm
   */
  save(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    const formObj = {
      email: formValue.email,
      password: formValue.password,
      type: this.type,
    };
    console.log(formObj);
    this.mail = formObj.email;
if(this.type == 1){
    this.loginService.studentLogin(formObj).subscribe(
      (res: any) => {
        if (res.statusCode == 200) {
          console.log(res);
          this.notifyService.showSuccess("Log In Successfully!!", "");
          this.NgbActiveModal.close();
          localStorage.setItem("key", res.body);
          this.loginResponseId = res.body;

          let navigationExtras: NavigationExtras = {
            state: {
              loginId: this.loginResponseId,
            },
          };

          this.router.navigateByUrl("/studentDashboard", navigationExtras);

        } else {
          console.log(res);
          this.notifyService.showError(res.body, "");

          if (res) {
            this.loginResponse = res;
          }
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
// company login
      if(this.type == 2){
        this.companyService.companyLogin(formObj).subscribe((res:any)=>{
          if(res.statusCode == 200){
            this.notifyService.showSuccess("Log In Successfully!!", "");
            this.NgbActiveModal.close();
            localStorage.setItem("companyLogIn", res.body);
            this.loginResponseId = res.body;

            let navigationExtras: NavigationExtras = {
              state: {
                loginId: this.loginResponseId,// send res.body in place of this.type
              },
            };
  // alert("company login");
            this.router.navigateByUrl('/companyDashboard',navigationExtras);

          } else{
            console.log(res);
            this.notifyService.showError(res.body, "");
  
            if (res) {
              this.loginResponse = res;
            }
          }
        });
//           this.notifyService.showSuccess("Log In Successfully!!", "");
//           this.NgbActiveModal.close();
//           localStorage.setItem("companyLogIn", 'companyLogIn');
//           this.loginResponseId = this.type;  // send res.body in place of this.type

//           let navigationExtras: NavigationExtras = {
//             state: {
//               loginId: this.type,// send res.body in place of this.type
//             },
//           };
// alert("company login");
//           this.router.navigateByUrl('/companyDashboard',navigationExtras);
      }

  }
  forgotPassword() {
    const formObj = {
      email: this.mail,
      type: this.type,
    };
    if (formObj && formObj.type && formObj.email && this.loginResponse) {
      console.log("login response", this.loginResponse);
      this.loginService.forgotPassword(formObj).subscribe(
        (res: any) => {
          if (res.statusCode == 200) {
            // alert("you will get message on mail");
            this.notifyService.showWarning(
              "You will get message on mail soon!!",
              ""
            );
            this.NgbActiveModal.close();
            const modalRef = this.modalService.open(ValidateSecretComponent);
            modalRef.componentInstance.obj = formObj;
          } else {
            // alert("something went wrong");
            this.notifyService.showError("Something went wrong!!", "");
          }
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      // alert("plz enter required information");
    }
    // this.modalService.open(ValidateSecretComponent);
  }
  close() {
    this.NgbActiveModal.close();
  }
  registerNow() {
    this.NgbActiveModal.close();
    this.router.navigateByUrl("/registration");
  }
}
