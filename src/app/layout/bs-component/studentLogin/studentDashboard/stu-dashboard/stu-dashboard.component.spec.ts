import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StuDashboardComponent } from './stu-dashboard.component';

describe('StuDashboardComponent', () => {
  let component: StuDashboardComponent;
  let fixture: ComponentFixture<StuDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StuDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StuDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
