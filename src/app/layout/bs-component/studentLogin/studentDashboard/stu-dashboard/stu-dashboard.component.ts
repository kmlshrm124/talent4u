import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
  SearchCountryField,
  TooltipLabel,
  CountryISO,
} from "ngx-intl-tel-input";

import { NotificationService } from "src/app/services/notification.service";
import {  NgbAccordion } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import { LoginService } from "src/app/services/login/login.service";


@Component({
  selector: "app-stu-dashboard",
  templateUrl: "./stu-dashboard.component.html",
  styleUrls: ["./stu-dashboard.component.scss"],
})
export class StuDashboardComponent implements OnInit {
  submitted: boolean = false;
  stuProfileForm: FormGroup;
  stuProfileSection2: FormGroup;
  stuProfileSection3: FormGroup;
  stuProfileSection4: FormGroup;
  stuProfileSection5: FormGroup;
  stuProfileSection6: FormGroup;
  stuProfileSection7: FormGroup;
  stuProfileSection8: FormGroup;

  matchPassword: any = {};
  file: any;
  public studentProfileObj: any = {};
  public isCollapsed = false;
  public cvStatus: boolean = false;
  fileToUpload: File = null;

  @ViewChild("labelImport", { static: true }) labelImport: ElementRef;
  @ViewChild("reset", { static: true }) Forms;
  @ViewChild("acc", { static: true }) accordionComponent: NgbAccordion;

  toggle(id: string): void {
    this.accordionComponent.toggle(id);
  }
  years = [
    "1990", "1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004",
    "2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019",
    "2020","2021",
  ];
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom,
  ];
  id: any;

  // phoneForm = new FormGroup({
  // 	phone: new FormControl("", [Validators.required])
  // });

  constructor(

    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private notify: NotificationService

  ) {
    if (typeof this.router.getCurrentNavigation().extras.state == "undefined") {
      this.id = localStorage.getItem("key");
      console.log("student id",this.id);
      // this.router.navigateByUrl('/dashboard');
    } else {
      this.id = this.router.getCurrentNavigation().extras.state.loginId;
      console.log("gamingObj", this.id);
    }
  }

  ngOnInit() {

    if (!this.id) {
      this.router.navigateByUrl("/dashboard");
      localStorage.removeItem("key");
    }

    this.stuProfileForm = this.fb.group({
      fName: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      mName: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      lName: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      fatherName: ["",[ Validators.minLength(6), Validators.maxLength(60)]],
      email: [""],
      gender: [""],
      mobile: [""],
      fatherMobile: [""],
      dob: [""],
      religion: ["", Validators.maxLength(20)],
      caste: ["",Validators.maxLength(20)],
      anyOtherDetail: ["", [Validators.minLength(2),Validators.maxLength(50)]],

    });
    console.log("stuProfileSection1", this.stuProfileForm.controls);

    this.stuProfileSection2 = this.fb.group({
      preference1: ["", [Validators.minLength(4), Validators.maxLength(30)]],
      preference2: ["", [Validators.minLength(4), Validators.maxLength(30)]],
      preference3: ["", [Validators.minLength(4), Validators.maxLength(30)]],
      address1: ["", [Validators.minLength(6),Validators.maxLength(30)]],
      address2: ["", [Validators.minLength(6),Validators.maxLength(30)]],
      importFile: [""],
      countryName: ["", [Validators.minLength(2),Validators.maxLength(10)]],
      stateName: ["",[Validators.minLength(2),Validators.maxLength(20)]],
      cityName: ["", [Validators.minLength(2),Validators.maxLength(20)]],
      zipCode: ["", [Validators.minLength(6),Validators.maxLength(10)]],
      gapAny: ["", [Validators.minLength(2),Validators.maxLength(50)]],
      anyOtherDetail: ["", [Validators.minLength(2),Validators.maxLength(50)]],

    });
    console.log("stuProfileSection2", this.stuProfileSection2.controls);

    this.stuProfileSection3 = this.fb.group({
      tenBoard: ["", [Validators.minLength(2),Validators.maxLength(20)]],
      tenPassYear: ["", [Validators.minLength(4),Validators.maxLength(4)]],
      tenGPA: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      tenLocation:["", [Validators.minLength(2),Validators.maxLength(20)]],
      tenSchool:["", [Validators.minLength(2),Validators.maxLength(50)]],
      tenSubject1: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      tenSubject2: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      tenSubject3: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      tenSubject4: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      tenSubject5: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      tenSubject6: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      anyOtherDetail: ["", [Validators.minLength(2),Validators.maxLength(50)]],

    });
    console.log("stuprofileSection3", this.stuProfileSection3.controls);

    this.stuProfileSection4 = this.fb.group({
      twelfthBoard: ["", [Validators.minLength(2),Validators.maxLength(20)]],
      twelfthPassYear: ["", [Validators.minLength(4),Validators.maxLength(4)]],
      twelfthGPA: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      twelfthSchool:["", [Validators.minLength(2),Validators.maxLength(50)]],
      twelfthLocation:["", [Validators.minLength(2),Validators.maxLength(20)]],
      twelfthSubject1: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      twelfthSubject2: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      twelfthSubject3: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      twelfthSubject4: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      twelfthSubject5: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      twelfthSubject6:["", [Validators.minLength(2),Validators.maxLength(30)]],
      anyOtherDetail: ["", [Validators.minLength(2),Validators.maxLength(50)]],

    });
    console.log("stuprofileSection4", this.stuProfileSection4.controls);

    this.stuProfileSection5 = this.fb.group({
      gradBoard: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      gradPassYear: ["", [Validators.minLength(4),Validators.maxLength(4)]],
      gradCollege: ["", [Validators.minLength(2),Validators.maxLength(50)]],
      gradLocation: ["", [Validators.minLength(2),Validators.maxLength(20)]],
      gradCourseName: ["", [Validators.minLength(2),Validators.maxLength(8)]],
      gradSpecialization: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      current: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_1: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_2: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_3: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_4: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_5: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_6: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_7: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_8: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      subject1:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject2:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject3:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject4:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject5:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject6:["", [Validators.minLength(2),Validators.maxLength(30)]],
      anyOtherDetail: ["", [Validators.minLength(2),Validators.maxLength(50)]],

    });
    console.log("stuprofileSection5", this.stuProfileSection5.controls);

    this.stuProfileSection6 = this.fb.group({
      postGradBoard: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      postGradPassYear: ["", [Validators.minLength(4),Validators.maxLength(4)]],
      postGradCollege: ["", [Validators.minLength(2),Validators.maxLength(50)]],
      postGradLocation: ["", [Validators.minLength(2),Validators.maxLength(20)]],
      postGradCourseName: ["", [Validators.minLength(2),Validators.maxLength(8)]],
      postGradSpecialization: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      current: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_1: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_2: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_3: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      sem_4: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      subject1:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject2:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject3:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject4:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject5:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject6:["", [Validators.minLength(2),Validators.maxLength(30)]],
      anyOtherDetail: ["", [Validators.minLength(2),Validators.maxLength(50)]],

    });
    console.log("stuprofileSection6", this.stuProfileSection6.controls);

    this.stuProfileSection7 = this.fb.group({
      diplomaBoard: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      diplomaPassYear: ["", [Validators.minLength(4),Validators.maxLength(4)]],
      diplomaCollege: ["", [Validators.minLength(2),Validators.maxLength(50)]],
      diplomaLocation: ["", [Validators.minLength(2),Validators.maxLength(20)]],
      diplomaCourseName: ["", [Validators.minLength(2),Validators.maxLength(8)]],
      diplomaSpecialization: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      diplomaGPA: ["", [Validators.minLength(2),Validators.maxLength(7)]],
      subject1:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject2:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject3:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject4:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject5:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject6:["", [Validators.minLength(2),Validators.maxLength(30)]],
      anyOtherDetail: ["", [Validators.minLength(2),Validators.maxLength(50)]],

    });
    console.log("stuprofileSection7", this.stuProfileSection7.controls);

    this.stuProfileSection8 = this.fb.group({

      certBoard:['', [Validators.minLength(2),Validators.maxLength(30)]],
      certPassYear:['', [Validators.minLength(4),Validators.maxLength(4)]],
      certCourseName:['', [Validators.minLength(2),Validators.maxLength(8)]],
      certSpecialization:['', [Validators.minLength(2),Validators.maxLength(30)]],
      certGPA:['', [Validators.minLength(2),Validators.maxLength(7)]],
      subject1:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject2:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject3:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject4:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject5:["", [Validators.minLength(2),Validators.maxLength(30)]],
      subject6:["", [Validators.minLength(2),Validators.maxLength(30)]],
      siteName: ["", [Validators.minLength(2),Validators.maxLength(30)]],
      scoreRanking: ["", [Validators.minLength(1),Validators.maxLength(10)]],
      curSem:['', [Validators.minLength(1),Validators.maxLength(4)]],
      lastSem:['', [Validators.minLength(1),Validators.maxLength(4)]],
      average:['', [Validators.minLength(1),Validators.maxLength(4)]],
      Status:['', [Validators.minLength(1),Validators.maxLength(1)]],
      NoPartIn:['', [Validators.minLength(1),Validators.maxLength(10)]],
      company:['', [Validators.minLength(2),Validators.maxLength(50)]],
      package:['', [Validators.minLength(2),Validators.maxLength(10)]],
      anyOtherDetail: ["", [Validators.minLength(2),Validators.maxLength(50)]],

    });
    console.log("stuprofileSection8", this.stuProfileSection8.controls);

    this.studentProfile();

  }


  studentProfile() {
    this.loginService.studentProfile(this.id).subscribe((res: any) => {
      console.log("profile check", res.body);
      this.studentProfileObj = res.body;
      console.log("studentProfileObj",this.studentProfileObj);
      if(this.studentProfileObj){
        this.formSectionSetValue();

      }

    });
  }
  
  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map((f) => f.name)
      .join(", ");
    this.fileToUpload = files.item(0);
  }

    // for reset form
    resetForm() {
      this.submitted = false;
      this.Forms.resetForm();
    }

  /**
   *
   * @param isValid
   * @param formValue
   * to save stuProfileForm
   */
  save(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    const formObj = {
      id: this.id,
      section: 1,
      fName: formValue.fName,
      mName: formValue.mName,
      lName: formValue.lName,
      dob:
        formValue.dob.year +
        "-" +
        formValue.dob.month +
        "-" +
        formValue.dob.day,
      gender: formValue.gender,
      fatherName: formValue.fatherName,
      religion: formValue.religion,
      caste: formValue.caste,
      mobile: formValue.mobile.e164Number,
      fatherMobile: formValue.fatherMobile.e164Number,
      customField1: formValue.anyOtherDetail,
    };
    console.log(formObj);
    this.loginService.studentProfileSection1(formObj).subscribe(
      (res: any) => {
        if(res.statusCode == 200){
        console.log("student profile section 1", res.body);
        this.resetForm();
        if(res.body.fatherName){
         this.studentProfile();
        } else{
      
        }
        this.notify.showSuccess("submitted successfully","");
        } else{
        this.notify.showError(res.statusCode,"");

        }
      },
      (error) => {
        console.log("student section1 error", error);
        this.notify.showError("Something went wrong","");
      }
    );
  }
  /**
   *
   * @param isValid
   * @param formValue
   * to save stuProfileForm
   */
  saveSection2(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    const formObj = {
      id: this.id,
      section: 2,
      jobLocation_1: formValue.preference1,
      jobLocation_2: formValue.preference2,
      jobLocation_3: formValue.preference3,
      cAddr_1: formValue.address1,
      cAddr_2: formValue.address2,
      cCity: formValue.cityName,
      cState: formValue.stateName,
      cZip: formValue.zipCode,
      cCountry: formValue.countryName,
      isCV: this.cvStatus,
      cvURL: this.file,
      gapAny: formValue.gapAny,
      customField1: formValue.anyOtherDetail,
    };
    console.log(formObj);
    this.loginService.studentProfileSection1(formObj).subscribe(
      (res: any) => {
        if(res.statusCode == 200){
        console.log("student profile section 2", res.body);
        this.resetForm();
        this.studentProfile();
        this.notify.showSuccess("submitted successfully","");
      } else{
        this.notify.showError(res.statusCode,"");

      }
      },
      (error) => {
        console.log("student section2 error", error);
        this.notify.showError("something went wrong","");
      }
    );
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save stuProfileForm
   */
  saveSection3(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;

    let arr = [];

    arr.push(
     {subject: formValue.tenSubject1},
     {subject:formValue.tenSubject2},
     {subject:formValue.tenSubject3},
     {subject: formValue.tenSubject4},
     {subject:formValue.tenSubject5},
     {subject:formValue.tenSubject6}
    );

    const tenQual = {
      // tenQual:{
      id: this.id,
      section: 3,
      tenStatus: true,
      tenBoard: formValue.tenBoard,
      tenPassYear: formValue.tenPassYear,
      tenGPA: formValue.tenGPA,
      tenSchool: formValue.tenSchool,
      tenLocation: formValue.tenLocation,
      customField1: formValue.anyOtherDetail,
      tenSubjects: arr,
    // }
    };

    console.log(tenQual);
    this.loginService.studentProfileSection1(tenQual).subscribe(
      (res: any) => {
        if(res.statusCode == 200){

        console.log("student profile section 3", res.body);
        this.notify.showSuccess("submitted successfully","");
        this.resetForm();
        this.studentProfile();
      } else{
        this.notify.showError(res.statusCode,"");
      }

      },
      (error) => {
        console.log("student section3 error", error);
        this.notify.showError("something went wrong","");

      }
    );
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save stuProfileForm
   */
  saveSection4(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    let arr = [];

    arr.push(
     {subject: formValue.twelfthSubject1},
     {subject:formValue.twelfthSubject2},
     {subject:formValue.twelfthSubject3},
     {subject: formValue.twelfthSubject4},
     {subject:formValue.twelfthSubject5},
     {subject:formValue.twelfthSubject6}
    );

    const twelfthQual = {
      // tenQual:{
      id: this.id,
      section: 4,
      twelfthStatus: true,
      twelfthBoard: formValue.twelfthBoard,
      twelfthPassYear: formValue.twelfthPassYear,
      twelfthGPA: formValue.twelfthGPA,
      twelfthSchool: formValue.twelfthSchool,
      twelfthLocation: formValue.twelfthLocation,
      customField1: formValue.anyOtherDetail,
      twelfthSubjects: arr,
    // }
    };
    console.log(twelfthQual);
    this.loginService.studentProfileSection1(twelfthQual).subscribe((res:any)=>{
      console.log("student profile section 4",res.body);
      this.resetForm();
      this.studentProfile();
      this.notify.showSuccess("submitted successfully","");
      }, (error)=>{
        console.log("student section4 error", error);
      });
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save stuProfileForm
   */
  saveSection5(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    let arr = [];
    let arr1 = [];

    arr.push(
     {subject: formValue.subject1},
     {subject:formValue.subject2},
     {subject:formValue.subject3},
     {subject: formValue.subject4},
     {subject:formValue.subject5},
     {subject:formValue.subject6}
    );

    let GPA ={
       current:  formValue.current ,
       sem_1:  formValue.sem_1,
       sem_2:  formValue.sem_2,
       sem_3:  formValue.sem_3,
       sem_4:  formValue.sem_4,
       sem_5:  formValue.sem_5,
       sem_6:  formValue.sem_6,
       sem_7:  formValue.sem_7,
       sem_8:  formValue.sem_8  
    }
   

    const gradQual = {
      // tenQual:{
      id: this.id,
      section: 5,
      gradStatus: true,
      gradBoard: formValue.gradBoard,
      gradPassYear: formValue.gradPassYear,
      gradCollege: formValue.gradCollege,
      gradCourseName : formValue.gradCourseName,
      gradSpecialization: formValue.gradSpecialization,
    gradLocation: formValue.gradLocation,
      gradSubjects: arr,
      gradGPA: GPA,
      customField1: formValue.anyOtherDetail,

    };
    console.log(gradQual);
    this.loginService.studentProfileSection1(gradQual).subscribe((res:any)=>{
      if(res.statusCode == 200){
        console.log("student profile section 5",res.body);
        this.resetForm();
        this.studentProfile();
        this.notify.showSuccess("submitted successfully","");
      }
      }, (error)=>{
        console.log("student section5 error", error);
      });
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save stuProfileForm
   */
  saveSection6(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    let arr = [];
    let arr1 = [];

    arr.push(
     {subject: formValue.subject1},
     {subject:formValue.subject2},
     {subject:formValue.subject3},
     {subject: formValue.subject4},
     {subject:formValue.subject5},
     {subject:formValue.subject6}
    );

    let GPA ={
      current:  formValue.current ,
      sem_1:  formValue.sem_1,
      sem_2:  formValue.sem_2,
      sem_3:  formValue.sem_3,
      sem_4:  formValue.sem_4, 
   }

    const postGradQual = {

      id: this.id,
      section: 6,
      postGradStatus: true,
      postGradBoard: formValue.postGradBoard,
      postGradSpecialization: formValue.postGradSpecialization,
      postGradPassYear: formValue.postGradPassYear,
      postGradCourseName: formValue.postGradCourseName,
      postGradCollege: formValue.postGradCollege,
      postGradLocation: formValue.postGradLocation,
      postGradSubjects: arr,
      postGradGPA: GPA,
      customField1: formValue.anyOtherDetail,

    };
    console.log(postGradQual);
    this.loginService.studentProfileSection1(postGradQual).subscribe((res:any)=>{
      if(res.statusCode == 200){
        console.log("student profile section post graduate",res.body);
        this.resetForm();
        this.studentProfile();
this.notify.showSuccess("Submitted successfully","");
      } else{
        console.log("something went wrong in post graduate section", res.body);
        this.notify.showError(res.statusCode,"");
      }
      }, (error)=>{
        console.log("student post graduate section error", error);
        this.notify.showError("Something went wrong","");

      });
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save stuProfileForm
   */
  saveSection7(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    let arr = [];

    arr.push(
     {subject: formValue.subject1},
     {subject:formValue.subject2},
     {subject:formValue.subject3},
     {subject: formValue.subject4},
     {subject:formValue.subject5},
     {subject:formValue.subject6}
    );

    const diplomaQual = {

      id: this.id,
      section: 7,
      diplomaStatus: true,
      diplomaBoard: formValue.diplomaBoard,
      diplomaCourseName: formValue.diplomaCourseName,
      diplomaSpecialization: formValue.diplomaSpecialization,
      diplomaPassYear: formValue.diplomaPassYear,
      diplomaCollege: formValue.diplomaCollege,
      diplomaLocation: formValue.diplomaLocation,
      diplomaSubjects: arr,
      diplomaGPA: formValue.diplomaGPA,
      customField1: formValue.anyOtherDetail,

    };
    console.log(diplomaQual);
    this.loginService.studentProfileSection1(diplomaQual).subscribe((res:any)=>{
      if(res.statusCode == 200){
      console.log("student profile section diploma",res.body);
      this.notify.showSuccess("Submitted successfully","");
      this.resetForm();
      this.studentProfile();
      } else{
        console.log("Error in section diploma", res.body);
        this.notify.showError(res.statusCode,"");
      }
      }, (error)=>{
        console.log("error in section diploma", error);
        this.notify.showError("Something went wrong","");

      });
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save stuProfileForm
   */
  saveSection8(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    let arr = [];
    let competitiveScoreArr = [];
    let attendancePercentageArr = [];
    let placementEligArr = [];
    let placedArr = [];
    arr.push(
     {subject: formValue.subject1},
     {subject:formValue.subject2},
     {subject:formValue.subject3},
     {subject: formValue.subject4},
     {subject:formValue.subject5},
     {subject:formValue.subject6}
    );

    competitiveScoreArr.push(
      { siteName:  formValue.siteName },
      { scoreRanking:  formValue.scoreRanking  },
    );

    placementEligArr.push(
      { Status: formValue.Status },
      { NoPartIn: formValue.NoPartIn }
    );
      placedArr.push(
        { company: formValue.company },
        { package : formValue.package }
        );
    attendancePercentageArr.push(
      { curSem: formValue.curSem },
      { lastSem: formValue.curSem },
      { average: formValue.average }
    );

    const certQual = {

      id: this.id,
      section: 8,
      certStatus: true,
      certBoard: formValue.certBoard,
      certPassYear: formValue.certPassYear,
      certCourseName: formValue.certCourseName,
      certSpecialization: formValue.certSpecialization,
      certGPA: formValue.certGPA,
      certSubjects: arr,
      competitiveScore: competitiveScoreArr,
      attendancePercentage: attendancePercentageArr,
      placementElig: placementEligArr,
      placed: placedArr,
      paperBack:"paper name",
      customField1: formValue.anyOtherDetail,

    };
    console.log(certQual);
    this.loginService.studentProfileSection1(certQual).subscribe((res:any)=>{
      console.log("student profile section 8",res.body);
      this.notify.showSuccess("Submitted successfully","");
      this.resetForm();
      this.studentProfile();

      }, (error)=>{
        console.log("student section8 error", error);
        this.notify.showError("something weng wrong","");
      });
  }
  /** image conversion in base64 format */
  fileselected(event) {
    if (event.target.files.length) {
      let file = event.target.files[0];
      console.log(file);
      var myReader: FileReader = new FileReader();
      myReader.readAsDataURL(file);
      myReader.onloadend = (e) => {
        console.log(myReader.result);
        this.file = myReader.result;
        if (this.file) {
          this.cvStatus = true;
        }
      };
    }
  }



formSectionSetValue(){

  console.log("setvalue1",this.stuProfileForm.controls['fName'].patchValue(this.studentProfileObj.fName));
  this.stuProfileForm.controls['mName'].patchValue(this.studentProfileObj.mName);
  this.stuProfileForm.controls['fatherName'].patchValue(this.studentProfileObj.fatherName);
  this.stuProfileForm.controls['lName'].patchValue(this.studentProfileObj.lName);
  this.stuProfileForm.controls['religion'].patchValue(this.studentProfileObj.religion);
  this.stuProfileForm.controls['caste'].patchValue(this.studentProfileObj.caste);
  this.stuProfileForm.controls['dob'].patchValue(this.studentProfileObj.dob);
  console.log("mobile console",this.stuProfileForm.controls['mobile'].patchValue(this.studentProfileObj.mobile.slice(3)));
  this.stuProfileForm.controls['fatherMobile'].patchValue(this.studentProfileObj.fatherMobile.slice(3));
  this.stuProfileForm.controls['gender'].patchValue(this.studentProfileObj.gender);
  this.stuProfileForm.controls['anyOtherDetail'].patchValue(this.studentProfileObj.customField1);

// console.log("mobile parse",(this.studentProfileObj.mobile.toString()));

  this.stuProfileSection2.controls['preference1'].patchValue(this.studentProfileObj.jobLocation_1);
  this.stuProfileSection2.controls['preference2'].patchValue(this.studentProfileObj.jobLocation_2);
  this.stuProfileSection2.controls['preference3'].patchValue(this.studentProfileObj.jobLocation_3);
  this.stuProfileSection2.controls['address1'].patchValue(this.studentProfileObj.cAddr_1);
  this.stuProfileSection2.controls['address2'].patchValue(this.studentProfileObj.cAddr_2);
  this.stuProfileSection2.controls['countryName'].patchValue(this.studentProfileObj.cCountry);
  this.stuProfileSection2.controls['stateName'].patchValue(this.studentProfileObj.cState);
  this.stuProfileSection2.controls['zipCode'].patchValue(this.studentProfileObj.cZip);
  this.stuProfileSection2.controls['cityName'].patchValue(this.studentProfileObj.cCity);
  this.stuProfileSection2.controls['gapAny'].patchValue(this.studentProfileObj.gapAny);
  console.log("filetoupload",this.labelImport.nativeElement.innerText= this.studentProfileObj.cvURL);


  this.stuProfileSection3.controls['tenBoard'].patchValue(this.studentProfileObj.tenQual.tenBoard);
  this.stuProfileSection3.controls['tenGPA'].patchValue(this.studentProfileObj.tenQual.tenGPA);
  this.stuProfileSection3.controls['tenLocation'].patchValue(this.studentProfileObj.tenQual.tenLocation);
  this.stuProfileSection3.controls['tenPassYear'].patchValue(this.studentProfileObj.tenQual.tenPassYear);
  this.stuProfileSection3.controls['tenSchool'].patchValue(this.studentProfileObj.tenQual.tenSchool);
  this.stuProfileSection3.controls['tenSubject1'].patchValue(this.studentProfileObj.tenQual.tenSubjects[0].subject);
  this.stuProfileSection3.controls['tenSubject2'].patchValue(this.studentProfileObj.tenQual.tenSubjects[1].subject);
  this.stuProfileSection3.controls['tenSubject3'].patchValue(this.studentProfileObj.tenQual.tenSubjects[2].subject);
  this.stuProfileSection3.controls['tenSubject4'].patchValue(this.studentProfileObj.tenQual.tenSubjects[3].subject);
  this.stuProfileSection3.controls['tenSubject5'].patchValue(this.studentProfileObj.tenQual.tenSubjects[4].subject);
  this.stuProfileSection3.controls['tenSubject6'].patchValue(this.studentProfileObj.tenQual.tenSubjects[5].subject);

  this.stuProfileSection4.controls['twelfthBoard'].patchValue(this.studentProfileObj.twelfthQual.twelfthBoard);
  this.stuProfileSection4.controls['twelfthGPA'].patchValue(this.studentProfileObj.twelfthQual.twelfthGPA);
  this.stuProfileSection4.controls['twelfthLocation'].patchValue(this.studentProfileObj.twelfthQual.twelfthLocation);
  this.stuProfileSection4.controls['twelfthPassYear'].patchValue(this.studentProfileObj.twelfthQual.twelfthPassYear);
  this.stuProfileSection4.controls['twelfthSchool'].patchValue(this.studentProfileObj.twelfthQual.twelfthSchool);
  this.stuProfileSection4.controls['twelfthSubject1'].patchValue(this.studentProfileObj.twelfthQual.twelfthSubjects[0].subject);
  this.stuProfileSection4.controls['twelfthSubject2'].patchValue(this.studentProfileObj.twelfthQual.twelfthSubjects[1].subject);
  this.stuProfileSection4.controls['twelfthSubject3'].patchValue(this.studentProfileObj.twelfthQual.twelfthSubjects[2].subject);
  this.stuProfileSection4.controls['twelfthSubject4'].patchValue(this.studentProfileObj.twelfthQual.twelfthSubjects[3].subject);
  this.stuProfileSection4.controls['twelfthSubject5'].patchValue(this.studentProfileObj.twelfthQual.twelfthSubjects[4].subject);
  this.stuProfileSection4.controls['twelfthSubject6'].patchValue(this.studentProfileObj.twelfthQual.twelfthSubjects[5].subject);

  this.stuProfileSection5.controls['gradBoard'].patchValue(this.studentProfileObj.gradQual.gradBoard);
  this.stuProfileSection5.controls['current'].patchValue(this.studentProfileObj.gradQual.gradGPA.current);
  this.stuProfileSection5.controls['sem_1'].patchValue(this.studentProfileObj.gradQual.gradGPA.sem_1);
  this.stuProfileSection5.controls['sem_2'].patchValue(this.studentProfileObj.gradQual.gradGPA.sem_2);
  this.stuProfileSection5.controls['sem_3'].patchValue(this.studentProfileObj.gradQual.gradGPA.sem_3);
  this.stuProfileSection5.controls['sem_4'].patchValue(this.studentProfileObj.gradQual.gradGPA.sem_4);
  this.stuProfileSection5.controls['sem_5'].patchValue(this.studentProfileObj.gradQual.gradGPA.sem_5);
  this.stuProfileSection5.controls['sem_6'].patchValue(this.studentProfileObj.gradQual.gradGPA.sem_6);
  this.stuProfileSection5.controls['sem_7'].patchValue(this.studentProfileObj.gradQual.gradGPA.sem_7);
  this.stuProfileSection5.controls['sem_8'].patchValue(this.studentProfileObj.gradQual.gradGPA.sem_8);
  this.stuProfileSection5.controls['gradLocation'].patchValue(this.studentProfileObj.gradQual.gradLocation);
  this.stuProfileSection5.controls['gradPassYear'].patchValue(this.studentProfileObj.gradQual.gradPassYear);
  this.stuProfileSection5.controls['gradCourseName'].patchValue(this.studentProfileObj.gradQual.gradCourseName);
  this.stuProfileSection5.controls['gradCollege'].patchValue(this.studentProfileObj.gradQual.gradCollege);
  this.stuProfileSection5.controls['gradSpecialization'].patchValue(this.studentProfileObj.gradQual.gradSpecialization);
  this.stuProfileSection5.controls['subject1'].patchValue(this.studentProfileObj.gradQual.gradSubjects[0].subject);
  this.stuProfileSection5.controls['subject2'].patchValue(this.studentProfileObj.gradQual.gradSubjects[1].subject);
  this.stuProfileSection5.controls['subject3'].patchValue(this.studentProfileObj.gradQual.gradSubjects[2].subject);
  this.stuProfileSection5.controls['subject4'].patchValue(this.studentProfileObj.gradQual.gradSubjects[3].subject);
  this.stuProfileSection5.controls['subject5'].patchValue(this.studentProfileObj.gradQual.gradSubjects[4].subject);
  this.stuProfileSection5.controls['subject6'].patchValue(this.studentProfileObj.gradQual.gradSubjects[5].subject);

  
  this.stuProfileSection6.controls['postGradBoard'].patchValue(this.studentProfileObj.postGradQual.postGradBoard);
  this.stuProfileSection6.controls['current'].patchValue(this.studentProfileObj.postGradQual.postGradGPA.current);
  this.stuProfileSection6.controls['sem_1'].patchValue(this.studentProfileObj.postGradQual.postGradGPA.sem_1);
  this.stuProfileSection6.controls['sem_2'].patchValue(this.studentProfileObj.postGradQual.postGradGPA.sem_2);
  this.stuProfileSection6.controls['sem_3'].patchValue(this.studentProfileObj.postGradQual.postGradGPA.sem_3);
  this.stuProfileSection6.controls['sem_4'].patchValue(this.studentProfileObj.postGradQual.postGradGPA.sem_4);
  this.stuProfileSection6.controls['postGradLocation'].patchValue(this.studentProfileObj.postGradQual.postGradLocation);
  this.stuProfileSection6.controls['postGradPassYear'].patchValue(this.studentProfileObj.postGradQual.postGradPassYear);
  this.stuProfileSection6.controls['postGradCourseName'].patchValue(this.studentProfileObj.postGradQual.postGradCourseName);
  this.stuProfileSection6.controls['postGradCollege'].patchValue(this.studentProfileObj.postGradQual.postGradCollege);
  this.stuProfileSection6.controls['postGradSpecialization'].patchValue(this.studentProfileObj.postGradQual.postGradSpecialization);
  this.stuProfileSection6.controls['subject1'].patchValue(this.studentProfileObj.postGradQual.postGradSubjects[0].subject);
  this.stuProfileSection6.controls['subject2'].patchValue(this.studentProfileObj.postGradQual.postGradSubjects[1].subject);
  this.stuProfileSection6.controls['subject3'].patchValue(this.studentProfileObj.postGradQual.postGradSubjects[2].subject);
  this.stuProfileSection6.controls['subject4'].patchValue(this.studentProfileObj.postGradQual.postGradSubjects[3].subject);
  this.stuProfileSection6.controls['subject5'].patchValue(this.studentProfileObj.postGradQual.postGradSubjects[4].subject);
  this.stuProfileSection6.controls['subject6'].patchValue(this.studentProfileObj.postGradQual.postGradSubjects[5].subject);

  this.stuProfileSection7.controls['diplomaBoard'].patchValue(this.studentProfileObj.diplomaQual.diplomaBoard);
  this.stuProfileSection7.controls['diplomaLocation'].patchValue(this.studentProfileObj.diplomaQual.diplomaLocation);
  this.stuProfileSection7.controls['diplomaPassYear'].patchValue(this.studentProfileObj.diplomaQual.diplomaPassYear);
  this.stuProfileSection7.controls['diplomaCourseName'].patchValue(this.studentProfileObj.diplomaQual.diplomaCourseName);
  this.stuProfileSection7.controls['diplomaCollege'].patchValue(this.studentProfileObj.diplomaQual.diplomaCollege);
  this.stuProfileSection7.controls['diplomaSpecialization'].patchValue(this.studentProfileObj.diplomaQual.diplomaSpecialization);
  this.stuProfileSection7.controls['subject1'].patchValue(this.studentProfileObj.diplomaQual.diplomaSubjects[0].subject);
  this.stuProfileSection7.controls['subject2'].patchValue(this.studentProfileObj.diplomaQual.diplomaSubjects[1].subject);
  this.stuProfileSection7.controls['subject3'].patchValue(this.studentProfileObj.diplomaQual.diplomaSubjects[2].subject);
  this.stuProfileSection7.controls['subject4'].patchValue(this.studentProfileObj.diplomaQual.diplomaSubjects[3].subject);
  this.stuProfileSection7.controls['subject5'].patchValue(this.studentProfileObj.diplomaQual.diplomaSubjects[4].subject);
  this.stuProfileSection7.controls['subject6'].patchValue(this.studentProfileObj.diplomaQual.diplomaSubjects[5].subject);
  
  this.stuProfileSection8.controls['certBoard'].patchValue(this.studentProfileObj.certQual.certBoard);
  this.stuProfileSection8.controls['certCourseName'].patchValue(this.studentProfileObj.certQual.certCourseName);
  this.stuProfileSection8.controls['certGPA'].patchValue(this.studentProfileObj.certQual.certGPA);
  this.stuProfileSection8.controls['certPassYear'].patchValue(this.studentProfileObj.certQual.certPassYear);
  this.stuProfileSection8.controls['certSpecialization'].patchValue(this.studentProfileObj.certQual.certSpecialization);
  this.stuProfileSection8.controls['subject1'].patchValue(this.studentProfileObj.certQual.certSubjects[0].subject);
  this.stuProfileSection8.controls['subject2'].patchValue(this.studentProfileObj.certQual.certSubjects[1].subject);
  this.stuProfileSection8.controls['subject3'].patchValue(this.studentProfileObj.certQual.certSubjects[2].subject);
  this.stuProfileSection8.controls['subject4'].patchValue(this.studentProfileObj.certQual.certSubjects[3].subject);
  this.stuProfileSection8.controls['subject5'].patchValue(this.studentProfileObj.certQual.certSubjects[4].subject);
  this.stuProfileSection8.controls['subject6'].patchValue(this.studentProfileObj.certQual.certSubjects[5].subject);

  this.stuProfileSection8.controls['siteName'].patchValue(this.studentProfileObj.competitiveScore[0].siteName);
  this.stuProfileSection8.controls['scoreRanking'].patchValue(this.studentProfileObj.competitiveScore[1].scoreRanking);

  this.stuProfileSection8.controls['curSem'].patchValue(this.studentProfileObj.attendancePercentage[0].curSem);
  this.stuProfileSection8.controls['lastSem'].patchValue(this.studentProfileObj.attendancePercentage[1].lastSem);
  this.stuProfileSection8.controls['average'].patchValue(this.studentProfileObj.attendancePercentage[2].average);

  this.stuProfileSection8.controls['Status'].patchValue(this.studentProfileObj.placementElig[0].Status);
  this.stuProfileSection8.controls['NoPartIn'].patchValue(this.studentProfileObj.placementElig[1].NoPartIn);

  this.stuProfileSection8.controls['company'].patchValue(this.studentProfileObj.placed[0].company);
  this.stuProfileSection8.controls['package'].patchValue(this.studentProfileObj.placed[1].package);

}
panelClick(event){
console.log("panel log", event);
}
}
