import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { LoginService } from "src/app/services/login/login.service";
import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { NotificationService } from "../../../../../services/notification.service";
import { LoginComponent } from 'src/app/layout/bs-component/studentLogin/login/login.component';


@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.scss"],
})
export class ResetPasswordComponent implements OnInit {
  submitted: boolean = false;
  stuRegisterForm: FormGroup;
  mail: any;
  object:any={};

  @Input() obj;
  @ViewChild("closebutton", { static: true }) closebuttons;
  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private modalService: NgbModal,
    private notify: NotificationService,
    private NgbActiveModal: NgbActiveModal
  ) { 
   
  }

  ngOnInit() {
    this.object = this.obj;
    this.stuRegisterForm = this.fb.group({
      password: ["", Validators.required],
      cPassword: ["", Validators.required],
    });

  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save gamesForm
   */
  save(isValid, formValue) {

    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    const formObj = {
      password: formValue.password,
      email: this.object.email,
      type: this.object.type,
    };
    console.log(formObj);

    this.loginService.resetPassword(formObj).subscribe(
      (res: any) => {
        if (res.statusCode == 200) {
          console.log(res.body);
          this.NgbActiveModal.close();
          //  const modalRef = this.modalService.open(LoginComponent);
          // modalRef.componentInstance.obj = formObj;
          this.notify.showSuccess("Password successfully reseted", "");
         
        } else {
          console.log(res.body);
          this.notify.showError(res.body, "");
        }
      },
      (error) => {
        console.log(error);
        this.notify.showError(error.body, "");
      }
    );
  }
  open() {
    this.modalService.open(ResetPasswordComponent);
    this.closebuttons.nativeElement.click();
  }
  close(){
    this.NgbActiveModal.close();
  }
  public onSave() {}
}
