import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BsComponentComponent } from './bs-component.component';
import { StudentregisterationComponent } from '../registeration/studentRegistration/studentregisteration/studentregisteration.component';
import { StuDashboardComponent } from './studentLogin/studentDashboard/stu-dashboard/stu-dashboard.component';

const routes: Routes = [
    {
        path: '',
        component: BsComponentComponent
    },
    // {
    //     path:'studentRegistration',
    //     component: StudentregisterationComponent
    // },
    // {
    //     path:'studentDashboard',
    //     component: StuDashboardComponent
    // }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BsComponentRoutingModule {}
