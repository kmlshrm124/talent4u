import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule, NgbCalendar, NgbCalendarGregorian } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import { BsComponentRoutingModule } from './bs-component-routing.module';
import { BsComponentComponent } from './bs-component.component';
import {
    AlertComponent,
    ButtonsComponent,
    ModalComponent,
    CollapseComponent,
    DatePickerComponent,
    DropdownComponent,
    PaginationComponent,
    PopOverComponent,
    ProgressbarComponent,
    TabsComponent,
    RatingComponent,
    TooltipComponent,
    TimepickerComponent
} from './components';
import { PageHeaderModule } from '../../shared';
// import { StudentregisterationComponent } from '../registeration/studentRegistration/studentregisteration/studentregisteration.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { LoginComponent } from './studentLogin/login/login.component';
import { ValidateSecretComponent } from './studentLogin/validateSecret/validate-secret/validate-secret.component';
import { ResetPasswordComponent } from './studentLogin/resetPassword/reset-password/reset-password.component';
// import { StuDashboardComponent } from './studentLogin/studentDashboard/stu-dashboard/stu-dashboard.component';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
    imports: [
  
    CommonModule,
        BsComponentRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        PageHeaderModule,
        BsDropdownModule.forRoot(), NgxIntlTelInputModule,
        MatDialogModule,
        TranslateModule,




    ],
    declarations: [
        BsComponentComponent,
        ButtonsComponent,
        AlertComponent,
        ModalComponent,
        CollapseComponent,
        DatePickerComponent,
        DropdownComponent,
        PaginationComponent,
        PopOverComponent,
        ProgressbarComponent,
        TabsComponent,
        RatingComponent,
        TooltipComponent,
        TimepickerComponent,

        // StudentregisterationComponent,

        LoginComponent,
        ValidateSecretComponent,
        ResetPasswordComponent,
        // StuDashboardComponent,


    ],
    providers: [ NgbCalendarGregorian],
    entryComponents:[ModalComponent,LoginComponent, ValidateSecretComponent, ResetPasswordComponent],
})
export class BsComponentModule {}
