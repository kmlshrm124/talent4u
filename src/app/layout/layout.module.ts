import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule, NgbDatepickerModule, NgbCollapseModule, NgbAccordion, NgbAccordionModule, NgbAlertModule, NgbPaginationModule, NgbModal, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { BsComponentModule } from './bs-component/bs-component.module';
import { SignupModule } from '../signup/signup.module';
import {MatDialogModule} from '@angular/material/dialog';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { StudentregisterationComponent } from './registeration/studentRegistration/studentregisteration/studentregisteration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StuDashboardComponent } from './bs-component/studentLogin/studentDashboard/stu-dashboard/stu-dashboard.component';

import {MatCardModule} from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { CompanyDashboardComponent } from './company/company-dashboard/company-dashboard.component';
import { CompanyPositionsComponent } from './company/company-positions/company-positions.component';
import { CompanyAddPositionsComponent } from './company/company-add-positions/company-add-positions.component';
import { PageHeaderModule } from '../shared';
import { LandingPageComponent } from './landingPage/landing-page/landing-page.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PopUpComponent } from './company/popUp/pop-up/pop-up.component';




@NgModule({
    imports: [
    CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule,
        BsComponentModule,
        SignupModule,
        MatDialogModule,
        ReactiveFormsModule,
        FormsModule,
        NgbDatepickerModule,
        BsDropdownModule.forRoot(), NgxIntlTelInputModule,
        NgbCollapseModule,
        NgbAccordionModule,
        MatCardModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatIconModule,
        MatSelectModule,
        NgbAlertModule,
        NgbPaginationModule,
        MatExpansionModule,
        NgxSpinnerModule

      




        

    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, StudentregisterationComponent,StuDashboardComponent, CompanyDashboardComponent, CompanyPositionsComponent, CompanyAddPositionsComponent, PopUpComponent  ],
    entryComponents:[CompanyAddPositionsComponent, PopUpComponent ],
    providers:[]
})
export class LayoutModule {}
