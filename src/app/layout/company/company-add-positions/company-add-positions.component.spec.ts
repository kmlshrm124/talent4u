import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyAddPositionsComponent } from './company-add-positions.component';

describe('CompanyAddPositionsComponent', () => {
  let component: CompanyAddPositionsComponent;
  let fixture: ComponentFixture<CompanyAddPositionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyAddPositionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyAddPositionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
