import { Component, OnInit,ViewChild, ElementRef, PipeTransform, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { SearchCountryField, TooltipLabel, CountryISO } from 'ngx-intl-tel-input';
import { NgbAccordion, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CompanyService } from 'src/app/services/companyService/company.service';

@Component({
  selector: 'app-company-add-positions',
  templateUrl: './company-add-positions.component.html',
  styleUrls: ['./company-add-positions.component.scss']
})
export class CompanyAddPositionsComponent implements OnInit {

  @ViewChild('acc',{static:true}) accordionComponent: NgbAccordion;
  @ViewChild('acc1',{static:true}) accordion: NgbAccordion;
  @ViewChild('selectionProcess',{static:true}) accordionSelectProcess: NgbAccordion;

  @Input() public positionData;

  companyAddPosition:FormGroup;
  submitted: boolean = false;
  id:any;
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom,
  ];
  years = [
    "1990", "1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004",
    "2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019",
    "2020","2021",
  ];

  constructor(
    private fb: FormBuilder,
    private companyService: CompanyService,
    private router: Router,
    private notify: NotificationService,
    private ngbActiveModal: NgbActiveModal
  ) { }

  ngOnInit() {
    console.log("Position data console", this.positionData);
// setTimeout(()=>{
// this.ngbActiveModal.close(this.positionData);
// },10000);
    this.companyAddPosition = this.fb.group({

      companyName:["", [Validators.required, Validators.minLength(6), Validators.maxLength(12)]],
      noOpenPosn:["", [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      posnCode:["", [ Validators.minLength(3), Validators.maxLength(30)]],
      posnType:["", [Validators.minLength(1), Validators.maxLength(10)]],
      courseName:["", [ Validators.minLength(2), Validators.maxLength(20)]],
      specialization:["",[ Validators.minLength(2), Validators.maxLength(20)]],
      gender:[""],
      CurSem:["",[ Validators.minLength(1), Validators.maxLength(4)]],
      LastSem:["",[ Validators.minLength(1), Validators.maxLength(4)]],
      // pass year
      to:["",[ Validators.minLength(4), Validators.maxLength(4)]],
      from:["",[ Validators.minLength(4), Validators.maxLength(4)]],
      gapAcceptable:["",[ Validators.minLength(1), Validators.maxLength(10)]],
      tenGPA:["",[ Validators.minLength(2), Validators.maxLength(7)]],
      twelfthGPA:["",[ Validators.minLength(2), Validators.maxLength(7)]],
      gradGPA:["",[ Validators.minLength(2), Validators.maxLength(7)]],
      postGradGPA:["",[ Validators.minLength(2), Validators.maxLength(7)]],
      // competitive score
      siteName:["",[ Validators.minLength(2), Validators.maxLength(30)]],
      scoreRanking:["",[ Validators.minLength(1), Validators.maxLength(10)]],
      jobURL:["",[ Validators.minLength(10), Validators.maxLength(50)]],
      jobTitle:["",[ Validators.minLength(4), Validators.maxLength(30)]],
      jobLocation_1:["",[ Validators.minLength(2), Validators.maxLength(20)]],
      jobLocation_2:["",[ Validators.minLength(2), Validators.maxLength(20)]],
      jobLocation_3:["",[ Validators.minLength(2), Validators.maxLength(20)]],
      jobProfile:["",[ Validators.minLength(10), Validators.maxLength(200)]],
      skillReqd:["",[ Validators.minLength(10), Validators.maxLength(200)]],
      // selectionProcess
      step1:["",[ Validators.minLength(10), Validators.maxLength(30)]],
      step2:["",[ Validators.minLength(10), Validators.maxLength(30)]],
      step3:["",[ Validators.minLength(10), Validators.maxLength(30)]],
      step4:["",[ Validators.minLength(10), Validators.maxLength(30)]],
      step5:["",[ Validators.minLength(10), Validators.maxLength(30)]],
      reqFmStudents:["",[ Validators.minLength(10), Validators.maxLength(200)]],
      termCondition:["",[ Validators.minLength(10), Validators.maxLength(200)]],
      otherInstructions:["",[ Validators.minLength(10), Validators.maxLength(200)]],
      // CTC Offer
      CTC:["",[ Validators.minLength(10), Validators.maxLength(200)]],
      monthly:["",[ Validators.minLength(2), Validators.maxLength(7)]],
      // bond
      bondYN:["",[ Validators.minLength(1), Validators.maxLength(1)]],
      bondDetail:["",[ Validators.minLength(2), Validators.maxLength(20)]],
      reportingDateTime:["",[ Validators.minLength(3), Validators.maxLength(15)]],
      reportingLocation:["",[ Validators.minLength(2), Validators.maxLength(40)]],
      // PosnStatus
      Status:["",[ Validators.minLength(1), Validators.maxLength(10)]],
      StartDate:[""],
      EndDate:[""],
     
     
     
     
      

    });
    console.log("add position obj", this.companyAddPosition.controls);
  }

  toggle(id:string): void {
    this.accordionComponent.toggle(id);
    this.accordion.toggle(id);
    this.accordionSelectProcess.toggle(id);
  }
   /**
   *
   * @param isValid
   * @param formValue
   * to save stuProfileForm
   */
  save(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    const formObj = {
    companyId: localStorage.getItem("companyLogIn"),
    tenantID:"tenantID.com",
    companyName: formValue.companyName,
    noOpenPosn: formValue.noOpenPosn,
    posnCode: formValue.posnCode,
    posnType: formValue.posnType,
    minQual:{
      courseName: formValue.courseName,
      specialization: formValue.specialization,
    },
    gender: parseInt(formValue.gender),
    attendancePercentage:{
      CurSem: formValue.CurSem,
      LastSem: formValue.LastSem
    },
    passYear:{
      to: formValue.to,
      from: formValue.from
    },
    gapAcceptable: formValue.gapAcceptable,
    tenGPA: formValue.tenGPA,
    twelfthGPA: formValue.twelfthGPA,
    gradGPA: formValue.gradGPA,
    postGradGPA: formValue.postGradGPA,
    competitiveScore:{
      siteName: formValue.siteName,
      scoreRanking: formValue.scoreRanking,
    },
    jobURL: formValue.jobURL,
    jobTitle: formValue.jobTitle,
    jobLocation_1: formValue.jobLocation_1,
    jobLocation_2: formValue.jobLocation_2,
    jobLocation_3: formValue.jobLocation_3,
    jobProfile: formValue.jobProfile,
    skillReqd: formValue.skillReqd,

    selectionProcess:{
    step1: formValue.step1,
    step2: formValue.step2,
    step3: formValue.step3,
    step4: formValue.step4,
    step5: formValue.step5,
    },
    reqFmStudents: formValue.reqFmStudents,
    termCondition: formValue.termCondition,
    otherInstructions: formValue.otherInstructions,
    CTCOffer:{
      CTC: formValue.CTC,
      monthly: formValue.monthly,
    },
    bond:{
      bondYN: formValue.bondYN,
      bondDetail: formValue.bondDetail
    },
    reportingDateTime: formValue.reportingDateTime
    
    };
 
    console.log(formObj);
    this.companyService.createPosition(formObj).subscribe(
      (res: any) => {
        if(res.statusCode == 200){
        console.log("student profile section 1", res.body);
        this.notify.showSuccess("submitted successfully","");
        this.ngbActiveModal.close();
        } else{
        this.notify.showError(res.statusCode,"");

        }
      },
      (error) => {
        console.log("student section1 error", error);
        this.notify.showError("Something went wrong","");
      }
    );
  }

}
