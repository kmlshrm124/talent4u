import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyPositionsComponent } from './company-positions.component';

describe('CompanyPositionsComponent', () => {
  let component: CompanyPositionsComponent;
  let fixture: ComponentFixture<CompanyPositionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyPositionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyPositionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
