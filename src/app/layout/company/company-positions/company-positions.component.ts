import { Component, OnInit, PipeTransform, Output, EventEmitter } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompanyAddPositionsComponent } from '../company-add-positions/company-add-positions.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyService } from 'src/app/services/companyService/company.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PopUpComponent } from '../popUp/pop-up/pop-up.component';
import { NotificationService } from 'src/app/services/notification.service';




@Component({
  selector: 'app-company-positions',
  templateUrl: './company-positions.component.html',
  styleUrls: ['./company-positions.component.scss']
})
export class CompanyPositionsComponent implements OnInit {

  page = 1;
  pageSize = 4;
  collectionSize:any;
 
  filter = new FormControl('');
  companyId:any;
  companyData:any=[];


  constructor( private ngbModal: NgbModal, private companyService: CompanyService,
    private notify: NotificationService,
   private router:Router, private route:ActivatedRoute, private spinner:NgxSpinnerService
    ) {
      this.companyId = this.route.snapshot.paramMap.get("id");


  }
   

  ngOnInit() {

    if(this.companyId){
      this.getAllPositions();
    }
  }

  getAllPositions(){
    this.spinner.show();
    const obj = {
      companyId: this.companyId
    }
    this.companyService.getAllPositions(obj).subscribe((res:any)=>{
      if(res.statusCode == 200){
        this.companyData = res.body;
        this.collectionSize= res.body.length;
        this.spinner.hide();
        console.log("company all positions",res.body);
      }else{
        console.log("error status",res);
      }
    },(error)=>{
      console.log(error);
    });
  }






  // searchTable(text){
  //   console.log(text);
  
  //   return COUNTRIES.filter((country,i) => {
  //   const term =text.toLowerCase();
  //   if(term == country.name.toLowerCase()){
  //     console.log("COUNTRIES",COUNTRIES[i]);
  //     let arr1 = COUNTRIES[i];
  //     let arr = Object.keys(arr1).map((k) => arr1[k])
  //     console.log("arr", arr)
  //     this.countries = arr;
  //     console.log("this.countries", this.countries);
  //     // COUNTRIES.push(COUNTRIES[i]);

  //     return console.log(country.name);

  //   }
  //    return console.log(country.name.toLowerCase().includes(term));
  //   });
  // }


  addPositions(){

    const modalRef = this.ngbModal.open(CompanyAddPositionsComponent,  { size:'lg', windowClass: 'custom-class'});
    modalRef.result.then((result) => {
      if ( result === 'success' ) {
        //  this.refreshData(); // Refresh Data in table grid
  
        console.log("refresh api calls",result);
      } else{
        console.log("refresh api calls");
        this.getAllPositions();
      }
    }, (reason) => {
      console.log("refresh api not call", reason);
    });
  }

  editPositions(positionData){
    alert(positionData);
  const modalRef =  this.ngbModal.open(CompanyAddPositionsComponent,   { size: 'lg' });
  modalRef.componentInstance.positionData = positionData;
  modalRef.result.then((result) => {
    if ( result === 'success' ) {
      //  this.refreshData(); // Refresh Data in table grid

      console.log("refresh api calls",result);
    } else{
      console.log("refresh api calls");
    }
  }, (reason) => {
    console.log("refresh api not call", reason);
  });
 
  }
  deletePositions(openPositionId){

    const deleteObj={
      "companyId":this.companyId,
      "openPositionId":openPositionId
    }
   
  const modalRef =  this.ngbModal.open(PopUpComponent, { centered: true , size: 'sm' });
  modalRef.result.then((result) => {
    if ( result == true ) {
       this.companyService.deleteOpenPosition(deleteObj).subscribe((res:any)=>{
      if(res.statusCode == 200){
        this.notify.showSuccess("Deleted Successfully","");
        this.getAllPositions();
      }else{
        this.notify.showError("Not deleted yet","");
      }
    });
      console.log("refresh api calls",result);
    } else if(result == false){
      console.log("refresh false api calls");
    }
  }, (reason) => {
    console.log("refresh api not call", reason);
  });
 
  }
  back(){
    this.router.navigate(['/companyDashboard']);
  }
}
