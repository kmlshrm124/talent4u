import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.scss']
})
export class PopUpComponent implements OnInit {

  constructor( private ngbActiveModal:NgbActiveModal) { }

  ngOnInit() {
    // setTimeout(()=>{
    //   this.ngbActiveModal.close();
    // },10000);
  }
delete(){
  this.ngbActiveModal.close(true);
}
cancel(){
  this.ngbActiveModal.close(false);
}
}
