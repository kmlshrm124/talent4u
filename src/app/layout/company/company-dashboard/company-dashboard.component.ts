import { Component, OnInit,ViewChild, ElementRef, PipeTransform } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoginService } from 'src/app/services/login/login.service';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { SearchCountryField, TooltipLabel, CountryISO } from 'ngx-intl-tel-input';
import { CompanyService } from 'src/app/services/companyService/company.service';



@Component({
  selector: 'app-company-dashboard',
  templateUrl: './company-dashboard.component.html',
  styleUrls: ['./company-dashboard.component.scss']
})
export class CompanyDashboardComponent implements OnInit {

  companyProfileForm:FormGroup;
  submitted: boolean = false;
  companyProfileObj:any={};
  id:any;
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom,
  ];
  years = [
    "1990", "1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004",
    "2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019",
    "2020","2021",
  ];

 
  constructor(

    private fb: FormBuilder,
    private companyService: CompanyService,
    private router: Router,
    private notify: NotificationService

  )  {
    if (typeof this.router.getCurrentNavigation().extras.state == "undefined") {
       this.id = localStorage.getItem("companyLogIn");
      // this.router.navigateByUrl('/dashboard');
    } else {
      this.id = this.router.getCurrentNavigation().extras.state.loginId;
      console.log("gamingObj", this.id);
    }


   }



  ngOnInit() {

    if (!this.id) {
      this.router.navigateByUrl("/dashboard");
      localStorage.removeItem("companyLogIn");
    }

    this.companyProfileForm = this.fb.group({
      fName: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      mName: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      lName: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      email: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(50)]],
      officialMail: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(50)]],
      mobile: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      mobile2: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      establishedYear: [""],
      companyName:["",[Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      industryType:["",[Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      employeeCount:["",[ Validators.minLength(1), Validators.maxLength(6)]],
      cAddr_1:["",[ Validators.minLength(2), Validators.maxLength(30)]],
      cAddr_2:["",[ Validators.minLength(2), Validators.maxLength(30)]],
      cCity:["",[ Validators.minLength(2), Validators.maxLength(20)]],
      cState:["",[ Validators.minLength(2), Validators.maxLength(20)]],
      cZip:["",[ Validators.minLength(1), Validators.maxLength(10)]],
      cCountry:["",[ Validators.minLength(2), Validators.maxLength(10)]],
      salesRep:["",[ Validators.minLength(2), Validators.maxLength(30)]],
      acctMgr:["",[ Validators.minLength(2), Validators.maxLength(30)]],
    

      // fatherName: ["",[ Validators.minLength(6), Validators.maxLength(60)]],
      // gender: [""],
      // fatherMobile: [""],

    });
    this.companyProfile();
  }

   /**
   *
   * @param isValid
   * @param formValue
   * to save companyProfileForm
   */
  save(isValid, formValue) {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    const formObj = {
       id: this.id,
      // section: 1,
      fName: formValue.fName,
      mName: formValue.mName,
      lName: formValue.lName,
      email: formValue.email,
      officialMail: formValue.officialMail,
      mobile: formValue.mobile.e164Number,
      officialMobile:formValue.mobile2.e164Number,
      establishedYear: formValue.establishedYear,
      comapnyName: formValue.companyName,
      industryType: formValue.industryType,
      employeeCount: formValue.employeeCount,
      cAddr_1: formValue.cAddr_1,
      cAddr_2: formValue.cAddr_2,
      cCity: formValue.cCity,
      cState: formValue.cState,
      cZip: formValue.cZip,
      cCountry: formValue.cCountry,
      salesRep: formValue.salesRep,
      acctMgr: formValue.acctMgr

    };
    console.log(formObj);
    this.companyService.companyProfileUpdate(formObj).subscribe(
      (res: any) => {
        if(res.statusCode == 200){
        console.log("company profile update", res.body);
        this.companyProfileForm.reset();
        this.formSectionSetValue();
        this.notify.showSuccess("submitted successfully","");
        } else{
        this.notify.showError(res.statusCode,"");

        }
      },
      (error) => {
        console.log("company profile update", error);
        this.notify.showError("Something went wrong","");
      }
    );
  }

  companyProfile() {
    this.companyService.getCompanyProfilelById(this.id).subscribe((res: any) => {
      console.log("profile check", res.body);
      this.companyProfileObj = res.body;
      console.log("companyProfileObj",this.companyProfileObj);
      if(this.companyProfileObj){
        this.formSectionSetValue();

      }

    });
  }

  companyPosition(){
    this.router.navigate(['/companyDashboard/positions',this.id]);
  }
  formSectionSetValue(){

    this.companyProfileForm.controls['fName'].patchValue(this.companyProfileObj.fName);
  this.companyProfileForm.controls['mName'].patchValue(this.companyProfileObj.mName);
  this.companyProfileForm.controls['lName'].patchValue(this.companyProfileObj.lName);
  this.companyProfileForm.controls['email'].patchValue(this.companyProfileObj.email);
  this.companyProfileForm.controls['officialMail'].patchValue(this.companyProfileObj.officialMail);
  this.companyProfileForm.controls['companyName'].patchValue(this.companyProfileObj.companyName);
  this.companyProfileForm.controls['industryType'].patchValue(this.companyProfileObj.industryType);
  this.companyProfileForm.controls['email'].patchValue(this.companyProfileObj.email);
  this.companyProfileForm.controls['establishedYear'].patchValue(this.companyProfileObj.establishedYear);
  this.companyProfileForm.controls['cAddr_1'].patchValue(this.companyProfileObj.cAddr_1);
  this.companyProfileForm.controls['cAddr_2'].patchValue(this.companyProfileObj.cAddr_2);
  this.companyProfileForm.controls['cCountry'].patchValue(this.companyProfileObj.cCountry);
  this.companyProfileForm.controls['cState'].patchValue(this.companyProfileObj.cState);
  this.companyProfileForm.controls['cCity'].patchValue(this.companyProfileObj.cCity);
  this.companyProfileForm.controls['cZip'].patchValue(this.companyProfileObj.cZip);
  this.companyProfileForm.controls['salesRep'].patchValue(this.companyProfileObj.salesRep);
  this.companyProfileForm.controls['acctMgr'].patchValue(this.companyProfileObj.acctMgr);

  this.companyProfileForm.controls['mobile'].patchValue(this.companyProfileObj.mobile.slice(3));
  this.companyProfileForm.controls['mobile2'].patchValue(this.companyProfileObj.officialMobile.slice(3));

  }
}
