import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationExtras } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../../bs-component/studentLogin/login/login.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { style, state } from '@angular/animations';
import { LoginService } from 'src/app/services/login/login.service';




@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public pushRightClass: string;
    public logout:boolean=true;

    constructor( 
        private translate: TranslateService, public router: Router, 
        private modalService:NgbModal,private loginService:LoginService,
        ) {
            // if(localStorage.getItem('key') || localStorage.getItem('companyLogIn') ){
            //     this.logout= false;

            //     }
        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });

    }

    ngOnInit() {
        setInterval(()=>{
            if(localStorage.getItem('key') || localStorage.getItem('companyLogIn') ){
                this.logout= false;
                }
           },100); 
     
        this.pushRightClass = 'push-right';
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    studentRegister(){

        let id = 1;
//         localStorage.setItem("RegisterKey",this.companyRegisterKey);
//         alert("student register");

//         let navigationExtras: NavigationExtras = {
//             state: {
//                 companyRegisterKey: this.companyRegisterKey,
//             },
//           };

//           this.router.navigateByUrl("/studentRegistration", navigationExtras);
this.loginService.setRegisterKey(id);

this.router.navigate(['/registration', id]);

    }
    companyRegister(){

         let id = 2;
//         localStorage.setItem("RegisterKey",this.companyRegisterKey);
//         alert("company register");

//         let navigationExtras: NavigationExtras = {
//             state: {
//                 companyRegisterKey: this.companyRegisterKey,
//             },
//           };

//           this.router.navigateByUrl("/studentRegistration", navigationExtras);
          this.loginService.setRegisterKey(id);

this.router.navigate(['/registration', id]);

            }

    studentLogin(){
        // alert("modal");
        let type = 1;
      const modalRef = this.modalService.open(LoginComponent);
      modalRef.componentInstance.type = type;
      

    }
    companyLogin(){
        let type = 2;
        const modalRef = this.modalService.open(LoginComponent);
        modalRef.componentInstance.type = type;
    }
    logOut(){
        localStorage.removeItem('key');
        localStorage.removeItem('companyLogIn');
        this.logout = true;
        this.router.navigateByUrl('/dashboard');
    }


}
