import { Component, OnInit, ViewChild } from "@angular/core";
import {
  SearchCountryField,
  TooltipLabel,
  CountryISO,
} from "ngx-intl-tel-input";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms";
import { routerTransition } from "src/app/router.animations";
import { StudentRegisterService } from "src/app/services/studentRegister/student-register.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { LoginComponent } from "src/app/layout/bs-component/studentLogin/login/login.component";
import { NotificationService } from "src/app/services/notification.service";
import { LoginService } from "src/app/services/login/login.service";
import { CompanyService } from 'src/app/services/companyService/company.service';

@Component({
  selector: "app-studentregisteration",
  templateUrl: "./studentregisteration.component.html",
  styleUrls: ["./studentregisteration.component.scss"],
  animations: [routerTransition()],
})
export class StudentregisterationComponent implements OnInit {
  submitted: boolean = false;
  stuRegisterForm: FormGroup;
  passwordMatch:boolean;
  matchPassword: any = {};
  registerKey: any;
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom,
  ];

  // phoneForm = new FormGroup({
  // 	phone: new FormControl("", [Validators.required])
  // });
  @ViewChild("reset", { static: true }) Forms;

  constructor(
    private fb: FormBuilder,
    private stuRegisterService: StudentRegisterService,
    private loginService: LoginService,
    private router: Router,
    private modalService: NgbModal,
    private notify: NotificationService,
    private route: ActivatedRoute,
    private companyService: CompanyService
  ) {
    // if (typeof this.router.getCurrentNavigation().extras.state == "undefined") {
    //   this.companyRegisterKey = localStorage.getItem("RegisterKey");
    //   console.log("companyRegisterKey", this.companyRegisterKey);
    //   if(this.companyRegisterKey == null){
    //     this.companyRegisterKey=1;
    //   console.log("companyRegisterKey",this.companyRegisterKey);
    //   }

    // }else{
      // this.router.events.filters(event => event instanceof NavigationEnd)
      // .subscribe(event => 
      //  {
      //     this.currentRoute = event.url;          
      //     console.log(event);
      //  });

  

    this.loginService.getRegisterKey().subscribe((res: any) => {
      this.registerKey = res;
    });
    this.registerKey = this.route.snapshot.paramMap.get("id");
    console.log("register key", this.registerKey);
  }

  ngOnInit() {

    this.stuRegisterForm = this.fb.group({
      fName: [
        "",
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(30),
        ],
      ],
      mName: [
        "",
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(30),
        ],
      ],
      lName: [
        "",
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(30),
        ],
      ],
      companyName:["",
          // Validators.required
          // Validators.minLength(4),
          // Validators.maxLength(30),
    ],
      industryType:["",
      // Validators.required
      // Validators.minLength(4),
      // Validators.maxLength(30),
    ],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.minLength(10),
          Validators.maxLength(50),
        ],
      ],
      mobile: [
        "",
        [
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10),
        ],
      ],
      password: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),

        ]
      ],
      cPassword: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),

        ],
      ],
    });
    console.log(this.stuRegisterForm.value);
    this.stuRegisterForm.controls.password.valueChanges;
    
    this.loginService.getRegisterKey().subscribe((res: any) => {
      // this.res = res;
      console.log("registerKey VALUE", res);
      if(res == 2){
        this.stuRegisterForm.controls.companyName.setValidators([Validators.required, Validators.minLength(4)
        ,Validators.maxLength(30)]);
        this.stuRegisterForm.controls.industryType.setValidators([Validators.required, Validators.minLength(4)
        ,Validators.maxLength(30)]);
      }
    });

    if(this.registerKey == 2){
      this.stuRegisterForm.controls.companyName.setValidators([Validators.required, Validators.minLength(4)
      ,Validators.maxLength(30)]);
      this.stuRegisterForm.controls.industryType.setValidators([Validators.required, Validators.minLength(4)
      ,Validators.maxLength(30)]);
    }

    this.stuRegisterForm.controls.cPassword.valueChanges.subscribe((cPassword:any)=>{
      console.log("cPass", cPassword);
      if(this.stuRegisterForm.controls.password.value === cPassword){
           this.matchPassword=true;
      } else{
         this.matchPassword=false;
      }
    });
    this.stuRegisterForm.controls.password.valueChanges.subscribe((password:any)=>{
      console.log("pass", password);
      if(this.stuRegisterForm.controls.cPassword.value === password){
          this.matchPassword=true;
      } else{
        this.matchPassword=false;
      }
    });
  }


  checkPasswords(group: FormGroup) {
    // here we have the 'passwords' group
    let pass = group.get("password").value;
    let confirmPass = group.get("confirmPass").value;

    return pass === confirmPass ? null : { notSame: true };
  }

  changePreferredCountries() {
    this.preferredCountries = [CountryISO.India, CountryISO.Canada];
  }

  // for reset form
  resetForm() {
    this.submitted = false;
    this.Forms.resetForm();
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save gamesForm
   */
  save(isValid, formValue) {
    // this.spinner.show();
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) return;
    const formObj = {
      fName: formValue.fName,
      mName: formValue.mName,
      lName: formValue.lName,
      email: formValue.email,
      mobile: formValue.mobile.e164Number,
      password: formValue.password,
      tenantID: "tenantID.com",
    };
    const comapnyObj ={
      companyName: formValue.companyName,
      industryType: formValue.industryType
    }
    console.log(formObj);

    if (this.registerKey == 1) {
    this.stuRegisterService.StudentRegister(formObj).subscribe(
      (res: any) => {
        if (res.statusCode == 200) {
          let type = this.registerKey;
          console.log(res.body);
          this.notify.showSuccess("register successfully", "");
          this.resetForm();

            this.router.navigateByUrl("/dashboard");
            const modalRef = this.modalService.open(LoginComponent);
            modalRef.componentInstance.type = this.registerKey;
         
        } else {
          this.notify.showError(res.body, "");
        }
      },
      (error) => {
        console.log(error);
        this.notify.showError(error.body, "");
      }
    );
    }
if(this.registerKey == 2){
  let obj = {
    ...formObj,
    ...comapnyObj
};
  this.companyService.companyRegister(obj).subscribe((res:any)=>{
    if (res.statusCode == 200) {
      let type = this.registerKey;
      console.log(res.body);
      this.notify.showSuccess("register successfully", "");
      this.resetForm();

        this.router.navigateByUrl("/dashboard");
        const modalRef = this.modalService.open(LoginComponent);
        modalRef.componentInstance.type = this.registerKey;

    } else {
      this.notify.showError(res.body, "");
    }

  },(error)=>{
    console.log(error);
    this.notify.showError(error.body, "");
  });
}

  }

  passwordCheck(control){
if(control.value != null){
  var conPass = control.value;
  var pass = control.root.get('password');
  if(pass){
    var password = pass.value;
    if(conPass !== "" && password !== ""){
      if(conPass !== password){
        return{
          passwordValidity: true
        }
      } else{
        return null
      }
    }
  }
}
  }

}
