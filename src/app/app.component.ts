import { Component, OnInit } from '@angular/core';
import { LoginService } from './services/login/login.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor( private refreshService:LoginService) {
    }

    ngOnInit() {
        this.refreshApi();
        setInterval(()=>{
            this.refreshApi();

        }, 240000);
    }
   
    refreshApi(){
        this.refreshService.refreshApi().subscribe((res:any)=>{
            if(res.statusCode == 200){
console.log("refresh api success console", res.body);
            } else{
                console.log("refresh api failure", res.body);
            }
        }, (error)=>{
            console.log("refresh api error", error.body);
        });
    }
}
