export const environment = {
  production: true,
  base_url :"https://29s9vw7n4a.execute-api.us-east-1.amazonaws.com/prod/",
  studentRegister_url : "https://29s9vw7n4a.execute-api.us-east-1.amazonaws.com/prod/student/register",
  studentLogin_urk : "https://29s9vw7n4a.execute-api.us-east-1.amazonaws.com/prod/login",
  forgotPassword_url :"https://29s9vw7n4a.execute-api.us-east-1.amazonaws.com/prod/forgotPassword",
  validateSecretKey_url :"https://29s9vw7n4a.execute-api.us-east-1.amazonaws.com/prod/validateSecret",
  resetPassword_url : "https://29s9vw7n4a.execute-api.us-east-1.amazonaws.com/prod/resetPassword",
  refresh_url : "https://29s9vw7n4a.execute-api.us-east-1.amazonaws.com/prod/refresh",
};
