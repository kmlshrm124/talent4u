// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  base_url :"https://qrc2m4s4wc.execute-api.us-east-1.amazonaws.com/dev/",
  studentRegister_url : "https://qrc2m4s4wc.execute-api.us-east-1.amazonaws.com/dev/student/register",
  studentLogin_urk : "https://qrc2m4s4wc.execute-api.us-east-1.amazonaws.com/dev/login",
  forgotPassword_url :"https://qrc2m4s4wc.execute-api.us-east-1.amazonaws.com/dev/forgotPassword",
  validateSecretKey_url :"https://qrc2m4s4wc.execute-api.us-east-1.amazonaws.com/dev/validateSecret",
  resetPassword_url : "https://qrc2m4s4wc.execute-api.us-east-1.amazonaws.com/dev/resetPassword",
  refresh_url : "https://qrc2m4s4wc.execute-api.us-east-1.amazonaws.com/dev/refresh",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
